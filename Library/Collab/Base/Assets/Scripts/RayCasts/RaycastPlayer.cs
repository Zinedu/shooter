﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastPlayer : MonoBehaviour
{
    
    //Lo usare para indicarle el script recoil que estoy disparando
    public event EventHandler playershooting;
    public delegate void shot(GameObject hit);
    public static event shot OnIAShot;

    [SerializeField(), Range(0f, 1000f)]
    float range;

    //La fuerza del raycast. Predeterminado estara en 300
    public float force= 100f;
    //true si quieres ver la distancia que tiene el raycast en cuanto colisionas con alguien (lo entenderas si lo pruebas).
    public bool printDistanceHit;

    // Update is called once per frame
    void Update()
    {
        //RaycastHit rc;
        //rc.distance
        //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * rc.distance, Color.red);
        //Nota: me mataras con este caos del parent. Lo que hago aqui es coger el padre (este script esta localizado en la camara del player), i desde el padre, coger el child 2 que es el gameobject arma, y cuento si tiene hijos, que teoricamente tendria que tener 1, que es la arma actual que esta usando.
        //Si no tiene hijos significa que no esta usando ahora ninguna arma y no vale la pena usar el raycast.
        //Por que hago esto??? Para reutilizar codigo y para que sea compatible en caso de cambio de escenas. Si lo hago instanciando con un gameobject publico pues, ya sabes que pasa al recargar escena.
        if (Input.GetMouseButtonDown(0) && this.transform.parent.transform.GetChild(2).transform.childCount != 0)
        {
            RaycastHit hit;
            //Origen (Gameobject), direccion del raycast (teoricamente coge el rotation en caso de que escoges otro gameobject), out hit es un simple output, range el rango, y el 3 es la mascara que vas a ignorar, el 3 es un layer vacio que puedes usar para no ignorar nada.
            if (Physics.Raycast(transform.position, transform.forward, out hit, range, 3))
            {

                if(hit.collider.gameObject.tag == "Enemy")
                {
                    if (OnIAShot != null)
                    {
                        OnIAShot(hit.collider.gameObject);
                    }
                }

                if (printDistanceHit) Debug.DrawRay(transform.position, transform.forward * hit.distance, Color.yellow);
                else Debug.DrawRay(transform.position, transform.forward * range, Color.yellow);

                //AddForceAtPosition(fuerza,position); La fuerza tienes que ponerle el hit.normal porque le tienes que dar coordenadas del mundo, y la cantidad es la multiplicacion que hace.


                hit.transform.GetComponent<Rigidbody>().AddForceAtPosition(-hit.normal * force, hit.point);
                Debug.Log("Did Hit " + hit.collider);



            }
            else
            {
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * range, Color.white);
                //Debug.Log("Did not Hit");
            }
            //Nota: mirar de implementar DPI
            playershooting?.Invoke(this, EventArgs.Empty);
        }
       

    }
}
