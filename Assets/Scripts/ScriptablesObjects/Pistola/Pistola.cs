﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Pistola", menuName = "Pistola")]
public class Pistola : ScriptableObject
{
    public string nombre;
    public float velocidadDisparo;
    public GameObject prefab;
}
