﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Arma : MonoBehaviour
{
    public Pistola[] carga;
    public Transform armaPadre;

    private GameObject actualArma;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            Equipo(0);
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            Equipo(1);
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            Equipo(2);
        }
    }

    void Equipo(int numero)
    {
        // si intentamos equipar algo que ya esta equipado lo destruye y equipa lo que intentas equiparle con la funcion de abajo 
        if (actualArma != null)
        {
            Destroy(actualArma);
        }

        // instanciar toma el objeto que enviamos al juego, en funcion del inicio desde el que salimos, 
        //toma la posicion,rotacion y luego el arma que estamos enviado
        GameObject newArma = Instantiate(carga[numero].prefab, armaPadre.position, armaPadre.rotation, armaPadre) as GameObject;
        newArma.transform.localPosition = Vector3.zero;
        newArma.transform.localEulerAngles = Vector3.zero;

        actualArma = newArma;
    }
}
