﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerController : MonoBehaviour
{

    public float Velocidad_Mov;
    public float modificarCorrer;
    public float fuerzaSalto;
    public Camera normalCam;
    public Transform detectorSuelo;
    public LayerMask suelo;

    public bool isDead = false;

    private Transform ui_vidaBarra;

    private Rigidbody rb;

    //BaseFPE es base flotante privada o entera privada
    private float baseFPE;
    private float modificadorCarreraFPE = 1.5f;


    private void Start()
    {
        //vida_actual = max_vida;

        baseFPE = normalCam.fieldOfView;
        if (Camera.main != null) Camera.main.enabled = false;
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        //Axis
        float hmove = Input.GetAxisRaw("Horizontal");
        float vmove = Input.GetAxisRaw("Vertical");


        //Controles
        bool correr = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
        bool saltar = Input.GetKey(KeyCode.Space);

        //Estados
        //Sera true si golpemaos algo con 0.1 da igual la unidad por debajo de nuestro personaje
        bool isTerreno = Physics.Raycast(detectorSuelo.position, Vector3.down, 0.1f, suelo);
        bool isSaltar = saltar && isTerreno;
        // asi solo corre hacia adelante y no te permite correr mientras saltas
        bool isCorrer = correr && vmove > 0 && !isSaltar && isTerreno;


        //Salto
        if (isSaltar)
        {
            rb.AddForce(Vector3.up * fuerzaSalto);
        }

        //Movimiento
        Vector3 direction = new Vector3(hmove, 0 , vmove);
        direction.Normalize();

        //ajuste de la velocidad para correr, siempre que sea verdadero
        //siempre sera por defecto igual a nuestra velocidad que pusimos en el inspector
        //*= modificarCorrer es para multiplicarlo por la cantidad que le añadimos cuando estamos corriendo
        float ajusteVelocidad_Mov = Velocidad_Mov;
        if (isCorrer) ajusteVelocidad_Mov *= modificarCorrer;

        // asi podremos saltar verticalmente 
        Vector3 tarjetVelocity = transform.TransformDirection(direction) * ajusteVelocidad_Mov * Time.deltaTime;
        tarjetVelocity.y = rb.velocity.y;
        rb.velocity = tarjetVelocity;



        //Field of View
        // estamos modificando la velocidad con la camara para hacer como un efecto 
        if(isCorrer)
        {
            // con esto logramos que se haga la transicion de correr y caminar sin pequeños saltos entre si 
            normalCam.fieldOfView = Mathf.Lerp(normalCam.fieldOfView, baseFPE * modificadorCarreraFPE, Time.deltaTime * 8f);

        }
        else
        {
            normalCam.fieldOfView = Mathf.Lerp(normalCam.fieldOfView, baseFPE , Time.deltaTime * 8f);
        }
    }
}
