﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeMono : MonoBehaviour
{

    public int life= 100;

    // Start is called before the first frame update
    void Start()
    {   
        
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(life);
        if (life <= 0)
        {
            PartsSpawner();
            this.gameObject.SetActive(false);
        }
    }

    void PartsSpawner()
    {
        int nParts = Random.Range(1, 5);
        for (int i = 0; i < nParts; i++)
        {
            int randomPart = Random.Range(0, 6);
            switch (randomPart)
            {
                case 0:
                    Instantiate(Resources.Load("Corpse_part_A"), new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z), Quaternion.identity);
                    break;
                case 1:
                    Instantiate(Resources.Load("Corpse_part_B"), new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z), Quaternion.identity);
                    break;
                case 2:
                    Instantiate(Resources.Load("Corpse_part_C"), new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z), Quaternion.identity);
                    break;
                case 3:
                    Instantiate(Resources.Load("Debris_jointed"), new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z), Quaternion.identity);
                    break;
                case 4:
                    Instantiate(Resources.Load("Debris_jointedB"), new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z), Quaternion.identity);
                    break;
            }
        }
    }

}
